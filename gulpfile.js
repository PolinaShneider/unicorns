'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    rigger = require('gulp-rigger'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    gcmq = require('gulp-group-css-media-queries'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    imageminPngquant = require('imagemin-pngquant'),
    imageminJpegRecompress = require('imagemin-jpeg-recompress'),
    plumber = require('gulp-plumber'),
    removeFiles = require('gulp-remove-files'),
    rename = require("gulp-rename"),
    browserSync = require('browser-sync');

var path = {
    src: {
        style: 'css/src/main.scss',
        js: 'js/src/main.js',
        img: 'images_prebuild/**/*.*',
        img_import: 'import_prebuild/**/*.*',
        html: 'html/*.html'
    },
    build: {
        js: 'js/',
        style: 'css/',
        img: 'images/',
        img_import: 'import/',
        img_preload: 'images/**/*.*',
        html: ''
    },

    watch: {
        style: 'css/src/**/*.scss',
        js: 'js/**/*.js',
        html: 'html/**/*'
    }
};

gulp.task('html:build', function () {
    gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gcmq())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.build.style));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({suffix: ".min"}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.build.js));
});


gulp.task('image:build', function () {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img))
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imageminPngquant({quality: '80'}),
            imagemin.svgo({
                plugins: [{removeXMLProcInst: true}, {removeTitle: true}, {removeDesc: true}, {removeUselessDefs: true}, {convertTransform: true},
                    {collapseGroups: true}, {cleanupIDs: true}, {removeUnusedNS: true}, {removeDimensions: true}]
            })
        ]))
        .pipe(gulp.dest(path.build.img));
});

gulp.task('clear-image', function () {
    return gulp.src(path.src.img)
        .pipe(removeFiles());
});

gulp.task('image:build_import', function () {
    return gulp.src(path.src.img_import)
        .pipe(gulp.dest(path.build.img_import))
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imageminJpegRecompress({
                progressive: true,
                max: 80,
                min: 70
            }),
            imageminPngquant({quality: '80'}),
            imagemin.svgo({plugins: [{removeViewBox: true}]})
        ]))
        .pipe(gulp.dest(path.build.img_import));
});

gulp.task('clear-image', function () {
    return gulp.src(path.src.img)
        .pipe(removeFiles());
});


gulp.task('browser-sync', function () {
    var files = [
        '/page-footer/**/*.html',
        '/css/main.css',
        '/js/**/*.js'
    ];

    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('watch', function () {
    gulp.watch([path.watch.html], ['html:build']);
    gulp.watch([path.watch.js], ['js:build']);
    gulp.watch([path.watch.style], ['style:build']);

});


gulp.task('default', ['browser-sync', 'watch']);